// Fasta_to_raw 1.0
// Copyright (C) 2013  Federico Zambelli and Giulio Pavesi
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <math.h>
#include <map>
#include <cstdlib>

using namespace std;

int main(int argc, char **argv)
{
	if(argc < 2)
	{
		cerr << "\nSyntax: fasta_to_raw file1 [file2] ... [fileN]" << endl;
		exit(1);
	}

//	ofstream out("info.txt");

	for(unsigned int i = 1; i < argc; i++)
	{
		ifstream in(argv[i]);

		if(!in)
		{
			cerr << "\nCan't find " << argv[i] << endl;
			in.close();
			continue;
		}

		string line;

		getline(in,line);

		if(line[0] != '>')
		{
			cerr << "\nBad file format in: " << argv[i] << endl;
			in.close();
			continue;
		}

		string fout = line.substr(1).c_str();

		for(unsigned int k = 0; k < 3; k++)
			fout[k] = tolower(fout[k]);

		fout += ".raw";

		ofstream out2(fout.c_str());

		while(getline(in,line))
		{
			if(line[0] == '>')
			{
				out2.close();
				fout = line.substr(1).c_str();
					
				for(unsigned int k = 0; k < 3; k++)
                        		fout[k] = tolower(fout[k]);
				
				fout += ".raw";
				out2.open((fout.c_str()));
				continue;
			}

			for(unsigned int j = 0; j < line.size(); j++)
			{
				line[j] = toupper(line[j]);
	
				if(line[j] != 'N' && line[j] != 'A' && line[j] != 'C' && line[j] != 'G' && line[j] != 'T')
					line[j] = 'N';
			}

			out2 << line;
		}

		out2.close();
		in.close();
	}

	return 0;
}
